import { createMuiTheme } from '@material-ui/core';

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#61dafb',
      contrastText: '#ffffff',
      light: '#ffffff',
      dark: '#282c34'
    },
    background: {
      default: '#282c34'
    },
    text: {
      secondary: '#ffffff',
      hint: 'rgb(215, 146, 247)'
    }  
  }
})