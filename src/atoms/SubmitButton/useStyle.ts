import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyle = makeStyles((theme: Theme) => 
  createStyles({
    button: {
      width: 180,
      height: 50,
      color: 'rgba(0, 0, 0, 0.87)',
      borderRadius: 50,
      fontWeight: 'bold',
      background: `linear-gradient(60deg, ${theme.palette.primary.main}, ${theme.palette.background.default})`,
      transition: '.2s',
      '&:hover': {
        filter: 'hue-rotate(90deg)'
      }
    }
  })
) 