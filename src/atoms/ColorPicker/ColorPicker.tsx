import React from 'react';
import { useStyle } from '../CustomTextField/useStyle';
import DefaultColorPicker from 'material-ui-color-picker';

export interface Props {
  saveColor: (color: string) => void;
  color: string;
}

export const ColorPicker = ({ saveColor, color }: Props) => {
  const styles = useStyle();

  return (
    <DefaultColorPicker
      className={styles.textField}
      floatingLabelText={color}
      value={color}
      onChange={saveColor}
      setValue={saveColor}
      variant='outlined'
    />
  )
}