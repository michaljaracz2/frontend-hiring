import React from 'react';
import { render } from '@testing-library/react';
import { ColorPicker } from '../ColorPicker';
import { UnitTestWrapper } from 'common/UnitTestWrapper';

describe('<ColorPicker />', () => {
  it('when pass correct props, should correct match the snapshot', () => {
    const { baseElement } = render(
      <UnitTestWrapper>
        <ColorPicker
          saveColor={jest.fn(() => { })}
          color='#000'
        />
      </UnitTestWrapper>
    )
    expect(baseElement).toMatchSnapshot();
  })
})