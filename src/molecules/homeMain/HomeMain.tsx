import React from 'react';
import { HomeLink } from 'atoms/homeLink';
import { useStyle } from './useStyle';

export const HomeMain = () => {
  const styles = useStyle();
  return (
    <main className={styles.homeMain}>
      <p className={styles.mainTextBlock}>
        Simply tell as what you need in your new project.
      </p>
      <HomeLink path='/questionnaire' content='Go to questionnaire' />
    </main>
  )
}