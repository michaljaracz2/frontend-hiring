import React from 'react';
import reactLogo from 'assets/reactLogo.svg';
import { useStyle } from './useStyle';

export const HomeHeader = () => {
  const styles = useStyle();
  return (
    <header className={styles.homeHeader}>
      <img className={styles.headerLogo} src={reactLogo} alt="logo" />
    </header>
  )
}