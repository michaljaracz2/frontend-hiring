import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyle = makeStyles((theme: Theme) => 
  createStyles({
    headerWrapper: {
      flexGrow: 1,
    },
    headerAppBar: {
      background: '#61dafb',
    },
    headerButton: {
      marginRight: theme.spacing(2),
      color: '#282c34'
    },
  })
)