import React from 'react';
import { QuestionnaireForm } from 'organisms/questionnaireForm';
import { CustomAppBar } from 'molecules/customAppBar';

export const Questionnaire = () => {
  return (
    <div>
      <CustomAppBar/>
      <QuestionnaireForm />
    </div>
  )
}