import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

export const useStyle = makeStyles((theme: Theme) => 
  createStyles({
    summary: {
      textAlign: 'center'
    },
    summaryTextBlockWrapper: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexWrap: 'wrap',
      flexDirection: 'column',
      margin: '0 auto',
    },
    summaryTextBlock: {
      width: '220px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      color: theme.palette.text.secondary,
    },
    summarySpecialText: {
      width: '220px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      color: theme.palette.text.hint,
    },
    summaryTitle: {
      color: theme.palette.primary.main,
      fontSize: '24px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: 'auto',
      maxWidth: '100vw',
      height: '120px',
      animation: '$titleEffect infinite 1.5s linear'
    },
    '@keyframes titleEffect': {
      '0%': {
        filter: 'hue-rotate(90deg)',
      },
      '100%': {
        filter: 'hue-rotate(0)',
      },
    },
  })
)