import { useTypedSelector } from 'redux/store'
import { selectFormData } from 'redux/questionnaireForm/questionnaireFormSlice'

export const useSummary = () => {
  const formData = useTypedSelector(selectFormData);

  return {
    formData,
  }
}