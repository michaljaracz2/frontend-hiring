import React from 'react';
import { CustomAppBar } from 'molecules/customAppBar';
import { useStyle } from './useStyle'
import { useSummary } from './useSummary';

export function Summary() {
  const styles = useStyle();
  const { formData } = useSummary();

  return (
    <div className={styles.summary}>
      <CustomAppBar />
      <div className={styles.summaryTitle}>
        Summary
      </div>
      <div className={styles.summaryTextBlockWrapper}>
        <p className={styles.summaryTextBlock}>
          Project Name:
        </p>
        <p className={styles.summarySpecialText}>
          {formData.projectName}
        </p>
        <p className={styles.summaryTextBlock}>
          Convenient currency:
        </p>
        <p className={styles.summarySpecialText}>
          {formData.selectedCurrency}
        </p>
        <p className={styles.summaryTextBlock}>
          Uploaded file name:
        </p>
        <p className={styles.summarySpecialText}>
          {formData.uploadedFilename}
        </p>
        <p className={styles.summaryTextBlock}>
          Plan start date:
        </p>
        <p className={styles.summarySpecialText}>
          {formData.startDate}
        </p>
        <p className={styles.summaryTextBlock}>
          Hex color tone:
        </p>
        <p className={styles.summarySpecialText}>
          {formData.colorString}
        </p>
      </div>
    </div>
  )
}