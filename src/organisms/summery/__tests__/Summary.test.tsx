import React from 'react';
import { render } from '@testing-library/react';
import { Summary } from '../Summary';
import { UnitTestWrapper } from 'common/UnitTestWrapper';

describe('<Summary />', () => {
  it('should match the snapshot', () => {
    const { baseElement } = render(
      <UnitTestWrapper>
        <Summary />
      </UnitTestWrapper>
    );
    expect(baseElement).toMatchSnapshot();
  })
})