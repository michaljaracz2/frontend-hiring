import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Questionnaire } from './questionnaire';
import { Summary } from './summery'
import { Home } from './home';

export function Routing() {
  return (
    <BrowserRouter>
      <Switch>
        <Route component={Home} exact path='/' />
        <Route component={Summary} path='/summary' />
        <Route component={Questionnaire} path='/questionnaire' />
      </Switch>
    </BrowserRouter>
  )
}